{
  description = "Nix Homepage flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    nur.url = "github:nix-community/NUR";
  };

  outputs = { self, nixpkgs, flake-utils, nur }:
  flake-utils.lib.eachDefaultSystem (system : let
    pkgs = import nixpkgs { inherit system; overlays = [ nur.overlay ]; };
    script = { ... }@args: pkgs.nur.repos.ysndr.lib.wrap ({ shell = true; } // args);
    reqs = with pkgs; [
      hugo
      git
      tree
    ];
  in rec {
    devShell = pkgs.mkShell {
      name = "homepage-shell";
      buildInputs = reqs;
    };

    packages.test_local = script {
      name = "generate-homepage-local-test";
      paths = reqs ++ [ pkgs.gitlab-runner ];
      script = ''
        gitlab-runner exec docker pages
      '';
    };
    packages.compile = script {
      name = "generate-homepage";
      paths = reqs;
      script = ''
        hugo
        ${pkgs.tree}/bin/tree public -I "reveal-js|highlight-js|themes|sass|reveal-hugo"
      '';
    };

    defaultPackage = packages.compile;
    apps.compile = flake-utils.lib.mkApp { drv = packages.compile; exePath = ""; };
    apps.test_local = flake-utils.lib.mkApp { drv = packages.test_local; exePath = ""; };
    defaultApp = apps.compile;
  });
}
