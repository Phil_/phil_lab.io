---
author: Philipp Herzog
title: Reproduzierbare Builds mit Nix
date: 2021-02-08
outputs: "Reveal"
---

# Reproduzierbare Builds mit Nix

2021-02-08, Philipp Herzog

---



# Was ist Nix

{{% section %}}

---


## Paketmanager

---

Andere Paketmanager existieren bereits:

- Pacman 
- Flatpak
- apt/apt-get
- Homebrew
- Cygwin
- Steam
- ...

---

## Was unterscheidet Nix von andereren?

Nix ist:

* Reproduzierbar
* Deklarativ
* Zuverlässig

---

## Reproduzierbar?

- isolierte Umgebungen
- Keine ungelösten Abhängigkeiten
- Paket läuft maschinenunabhängig

---

## Deklarativ?

Vollkommene Beschreibung

Nicht *wie*, sondern *was*

---

## Zuverlässig?

* Pakete sind unabhängig voneinander
* Rollbacks

{{% /section %}}

---

{{% section %}}

# Wie funktioniert Nix

---

Bausteine von Nix:

* Der Nix store
* Die Sprache Nix
* Derivationen

---

Alle Pakete und Libraries befinden sich im Nix store

```sh
λ ❱ ll /nix/store | tail -5 | sed 's/.*1970  //'
zz96apki8hdymhcnz2sil6h2dj9fqgbh-binutils-wrapper-2.35.1.drv
zzddfyl442r6321cmpksbp5mw5844can-perl-5.32.0.drv
zzj4g12vkymprlay9bapm6k3yz4sz670-dbus-1.12.20.drv
zzmm1vw7sflrxjvlrvw8y6f63nfm2h9h-python3.8-ufl-2019.1.0.drv
zznxzs6hmwmflzch1f3miaxlz7yamjlb-source.drv
zzpg9g6zn622b6q5xlg9sybr1m7ax0rh-ninja-1.10.2.drv
zzv24zlvaqpsjz23fm6hld6ls52kh2lm-gnupg-2.2.24.drv
zzwpk8rn8vxc59prgrarpj7sqi22idcl-source.drv
zzy4nbkkcgh5bblkmj50br9s0jhddj1g-modes.r56303.tar.xz.drv
zzyfp2mnwr1jy96a1f062022w2x4rwmg-libXpm-3.5.13.drv
```

```sh
λ ❱ fd -t d -d 1 'gcc-[0-9.]+$' /nix/store | wc -l
9
λ ❱ fd -t d -d 1 'gcc-[0-9.]+$' /nix/store | sed 's/-/ /' | awk '{ print $2 }' | sort | uniq
gcc-10.2.0
gcc-9.3.0
```

---

```sh
λ ❱ fd -t d -d 1 'gcc-[0-9.]+$' /nix/store | head -1
/nix/store/3fdszyx6f4gri2fsqpd6rm8ra44ih8qb-gcc-10.2.0
```

```sh
λ ❱ nix-store --query --referrers /nix/store/3fdszyx6f4gri2fsqpd6rm8ra44ih8qb-gcc-10.2.0
/nix/store/3fdszyx6f4gri2fsqpd6rm8ra44ih8qb-gcc-10.2.0
/nix/store/fr5gpdf3m2nqq6g3v6x1vvgi7cg6px8b-gcc-wrapper-10.2.0
/nix/store/i3xa8qyqxpzkckgxfv9cvwfvcpi2khkp-maple-env-usr-target
/nix/store/idq73vbcpkcwpwj134a4a1sh5y840xzk-clang-wrapper-9.0.1
/nix/store/wjpilss63b9q73ywhkwzb8qmv5fp19l4-nix-shell-env
```

---

```sh
λ ❱ nix-store --query --graph /nix/store/3fdszyx6f4gri2fsqpd6rm8ra44ih8qb-gcc-10.2.0
digraph G {
"3fdszyx6f4gri2fsqpd6rm8ra44ih8qb-gcc-10.2.0" [label = "gcc-10.2.0", shape = box, style = filled, fillcolor = "#ff0000"];
"hayaj8q9rc7swx4llcjqymmg3zr525lx-glibc-2.32-25-dev" -> "3fdszyx6f4gri2fsqpd6rm8ra44ih8qb-gcc-10.2.0" [color = "black"];
"jpf7r0zvv7a8xlk6330g0qcnimwxqc7l-gcc-10.2.0-lib" -> "3fdszyx6f4gri2fsqpd6rm8ra44ih8qb-gcc-10.2.0" [color = "red"];
"m0xa5bz7vw7p43wi0jppvvi3c9vgqvp7-glibc-2.32-25" -> "3fdszyx6f4gri2fsqpd6rm8ra44ih8qb-gcc-10.2.0" [color = "green"];
"pvy77kjmkvcjfjhsva65cgi7v6hc1262-zlib-1.2.11" -> "3fdszyx6f4gri2fsqpd6rm8ra44ih8qb-gcc-10.2.0" [color = "blue"];
"hayaj8q9rc7swx4llcjqymmg3zr525lx-glibc-2.32-25-dev" [label = "glibc-2.32-25-dev", shape = box, style = filled, fillcolor = "#ff0000"];
"m0xa5bz7vw7p43wi0jppvvi3c9vgqvp7-glibc-2.32-25" -> "hayaj8q9rc7swx4llcjqymmg3zr525lx-glibc-2.32-25-dev" [color = "magenta"];
"y0s7p092x0n2y5zmrq1h211gkp71842f-glibc-2.32-25-bin" -> "hayaj8q9rc7swx4llcjqymmg3zr525lx-glibc-2.32-25-dev" [color = "burlywood"];
"yj57bwkj3jw1acsmn8aydp4jpzcpv0j0-linux-headers-5.10.4" -> "hayaj8q9rc7swx4llcjqymmg3zr525lx-glibc-2.32-25-dev" [color = "black"];
"jpf7r0zvv7a8xlk6330g0qcnimwxqc7l-gcc-10.2.0-lib" [label = "gcc-10.2.0-lib", shape = box, style = filled, fillcolor = "#ff0000"];
"m0xa5bz7vw7p43wi0jppvvi3c9vgqvp7-glibc-2.32-25" -> "jpf7r0zvv7a8xlk6330g0qcnimwxqc7l-gcc-10.2.0-lib" [color = "red"];
"m0xa5bz7vw7p43wi0jppvvi3c9vgqvp7-glibc-2.32-25" [label = "glibc-2.32-25", shape = box, style = filled, fillcolor = "#ff0000"];
"hn0qr5sk0bvvnjyg6r3yrkwbyl0gn953-libidn2-2.3.0" -> "m0xa5bz7vw7p43wi0jppvvi3c9vgqvp7-glibc-2.32-25" [color = "green"];
"hn0qr5sk0bvvnjyg6r3yrkwbyl0gn953-libidn2-2.3.0" [label = "libidn2-2.3.0", shape = box, style = filled, fillcolor = "#ff0000"];
"ysz0g8jwfcz90px73g4yd3i2wgcbxmlv-libunistring-0.9.10" -> "hn0qr5sk0bvvnjyg6r3yrkwbyl0gn953-libidn2-2.3.0" [color = "blue"];
"pvy77kjmkvcjfjhsva65cgi7v6hc1262-zlib-1.2.11" [label = "zlib-1.2.11", shape = box, style = filled, fillcolor = "#ff0000"];
"m0xa5bz7vw7p43wi0jppvvi3c9vgqvp7-glibc-2.32-25" -> "pvy77kjmkvcjfjhsva65cgi7v6hc1262-zlib-1.2.11" [color = "magenta"];
"y0s7p092x0n2y5zmrq1h211gkp71842f-glibc-2.32-25-bin" [label = "glibc-2.32-25-bin", shape = box, style = filled, fillcolor = "#ff0000"];
"m0xa5bz7vw7p43wi0jppvvi3c9vgqvp7-glibc-2.32-25" -> "y0s7p092x0n2y5zmrq1h211gkp71842f-glibc-2.32-25-bin" [color = "burlywood"];
"yj57bwkj3jw1acsmn8aydp4jpzcpv0j0-linux-headers-5.10.4" [label = "linux-headers-5.10.4", shape = box, style = filled, fillcolor = "#ff0000"];
"ysz0g8jwfcz90px73g4yd3i2wgcbxmlv-libunistring-0.9.10" [label = "libunistring-0.9.10", shape = box, style = filled, fillcolor = "#ff0000"];
}
```




---

### Der SSG Hugo

![hugo](hugo.png)

---

### SVM-Tool git

![git](git.png)

---

Pfad aufgebaut über Profile

```sh
λ ❱ echo $PATH | sed 's/:/\n/g'
/run/wrappers/bin
/home/nixos/.nix-profile/bin
/etc/profiles/per-user/nixos/bin
/nix/var/nix/profiles/default/bin
/run/current-system/sw/bin
```

```sh
λ ❱ which git
/etc/profiles/per-user/nixos/bin/git
```

```sh
λ ❱ ll /etc/profiles/per-user/nixos/bin | tail -5 | sed 's/.*1970  //'
zipinfo -> /nix/store/8whwal4zjhr6m4cdvm48snv6whbcnjxf-home-manager-path/bin/zipinfo
zotero -> /nix/store/8whwal4zjhr6m4cdvm48snv6whbcnjxf-home-manager-path/bin/zotero
zoxide -> /nix/store/8whwal4zjhr6m4cdvm48snv6whbcnjxf-home-manager-path/bin/zoxide
zsh -> /nix/store/8whwal4zjhr6m4cdvm48snv6whbcnjxf-home-manager-path/bin/zsh
zsh-5.8 -> /nix/store/8whwal4zjhr6m4cdvm48snv6whbcnjxf-home-manager-path/bin/zsh-5.8
```

---

## Die Sprache Nix

* Lazy Evaluation
    - Ausdrücke werden nur beim Zugriff ausgewertet
* Rein Funktional
    - keine Statements, sondern Ausdrücke
    - Vermeidung von Zustand
* Pur
    * allein Input bestimmt den Output, keine Seiteneffekte
        * "derivation" Aufruf
    
---

## Derivationen

#### "Rezepte" zum Aufbau von Paketen

* Input
    * alle Dependencies
* Anleitung zum Kompilieren
    * Skript / Aufruf von make / etc.
* Output
    * Kopieren der relevatenten Dateien in den Store
* Funktion "derivation" der Sprache Nix erstellt Datei im Nix Store

{{% /section %}}

---

{{% section %}}

# Wie nutze ich Nix?

* Bauen von Software
* Entwicklungsumgebungen
* CI
* Konfiguration von Systemen
    * Paketmanagment
    * Dotfiles
    * Nutzer / Gruppen
    * Kernel
    * Bootloader
    * jegliche andere Konfiguration
    
---

## Snippets

---

Setzen von Umgebungsvariablen
```nix
{ pkgs, ... }: let 
  username = "nixos";
  homeDirectory = "/home/${username}";
  lib = pkgs.stdenv.lib;
in rec {
  home = { 
    inherit username homeDirectory;
    stateVersion = "21.03";
    sessionVariables = {
      EDITOR = "${pkgs.neovim-nightly}/bin/nvim";
      PAGER = "${pkgs.page}/bin/page";
      MANPAGER = "${pkgs.page}/bin/page -C -e 'au User PageDisconnect sleep 100m|%y p|enew! |bd! #|pu p|set ft=man'";
    };
}
```

---

Ausschnitt aus der Nutzerkonfiguration
```nix
{
  programs = {
    gpg = {
      enable = true;
      settings.default-key = "BDCD0C4E9F252898";
    };

    rofi = {
      enable = true;
      package = pkgs.rofi-wayland;
    };
}
```

---

Ausschnitt aus meiner Host Konfiguration für den Arbeitslaptop
```nix
{
  imports = [ ./hardware-configuration.nix ];
  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  # set network interfaces
  networking.hostName = hostname;
  networking.wireless.enable = true;
  networking.wg-quick.interfaces = {
    mullvad = import ../vpn/mullvad.nix;
  };
  virtualisation.docker.enable = true;
}
```

---

Pipewire
```nix
{
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;
  };
}
```

---

CI
```yml
variables:
    GIT_SUBMODULE_STRATEGY: recursive
    
image:
    name: maelstroem/nixflk-docker
    entrypoint: ["/bin/sh", "-c"]
    
pages:
    stage: deploy
    only: 
        - main
    script:
        - nix run ./#compile
    artifacts:
        paths:
            - public
```

Die Datei `flake.nix` im gleichen Repo (`./`) definiert hier das "Target" `compile`.

{{% /section %}}

---

# Noch Fragen?
